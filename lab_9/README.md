
 Apa itu Cookie dan Session
Cookie:
- Small amount of information sent by a web server to a browser and then sent back by the browser on future page requests
- Cookies are used to:
  • Authentication
  • User tracking
  • Maintaining user preferences
- A cookie’s data consists of a single name/value pair (like dictionary),
sent in the header of the client’s HTTP GET or POST request
- Session cookie (default): temporary cookie
• Stored in the browser memory
• When the browser is closed, it will be erased
• Can’t be used for tracking long-term information
• Safer, only browser can access
- Persistent cookie:
• Stored in a file on the browser’s computer
• Can track long term information
• Less secure: users or programs can open cookie files

Session:
- an abstract concept to represent a series of HTTP request and responses between
a specific Web browser and server
  • HTTP doesn’t support the notion of a session
- How Sessions and Cookies work together:
• A session’s data is stored on the server (1 session per client).
• A cookie is data stored on the client, it may consist some session information

Session lebih aman dari pada cookie karena cookie dapat menyimpas informasi seperti id dan Password
walau di simpan dalam token, namun token tersebut bisa dipecahkan dan ini tersimpan di dalam browser
sedangkan session akan menyimpan token tersebut hingga interaksi user dengan website tersebut selesai.

Dapat menggunakan Cookie & Session

berikut contoh penyimpanan data dengan session pada lab 9:

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    try:
        response['drones'] = get_drones().json()
        response['soundcard'] = get_soundcard().json()
        response['optical'] = get_optical().json()
    except:
        print("Error")

berikut contoh penyimpanan pass dan id dengan Cookies
def cookie_profile(request):
    print ("# cookie profile ")
    # method ini untuk mencegah error ketika akses URL secara langsung
    if not is_login(request):
        print ("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        # print ("cookies => ", request.COOKIES)
        in_uname = request.COOKIES['user_login']
        in_pwd= request.COOKIES['user_password']

        # jika cookie diset secara manual (usaha hacking), distop dengan cara berikut
        # agar bisa masuk kembali, maka hapus secara manual cookies yang sudah diset
        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res =  render(request, html, response)
            return res
        else:
            print ("#login dulu")
            msg = "Kamu tidak punya akses :P "
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)
