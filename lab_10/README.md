cara models mengenali bahwa sebuah data itu milik user tertentu (lab10)

hal ini dapat dilihat pada model bagian pengguna, dikarenaka suatu file
akan dibedakan berdasarkan penggunanya, models akan menyimpan pengguna tersebut
dan menjadikannya sebagai parameter untuk pengambilan data lain.
dengan begitu models dapat membedakan pengguna satu dengan pengguna lainnya

berbeda dengan session dimana session memiliki semacam pemisah antara user satu dengan user lainnya
dimana session akan meminta terlebih dahulu token yang dimiliki oleh user,
baru server akan mengembalikan data yang dimili user tersebut sesuai dengan token yg mereka miliki
